package com.qaagility.controller;


import org.junit.Test;
import static org.junit.Assert.*;

public class CntWithALongerNameTest {
    @Test
    public void testCnt() throws Exception {

        int k= new CntWithALongerName().dButLonger(1,0);
        assertEquals("Error in divission with 0", Integer.MAX_VALUE, k);
        
    }

    @Test
    public void testCnt2() throws Exception {

        int k= new CntWithALongerName().dButLonger(9,3);
        assertEquals("Error in division", 3, k);
        
    }
}

