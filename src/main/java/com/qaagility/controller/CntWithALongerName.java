package com.qaagility.controller;

public class CntWithALongerName {

    public int dButLonger(int digitA, int digitB) {
        if(digitB==0){
            return Integer.MAX_VALUE;
        }
        else{
            return digitA / digitB;
        }
    }

}
